REGISTRATION RESTRICTION MODULE
--------------------------

This module allows a site admin to restrict user registration based on 
whitelisting or blacklisting particular email domains.

An example use case is a school or organization that wants to restrict account 
creation to emails from their own domain.


REQUIREMENTS
------------

  * Drupal 7.x


INSTALLATION
------------

Install and enable the module. More specific instructions can be found here: 
http://drupal.org/documentation/install/modules-themes/modules-7

Configuration can be found at: 
[your_site_base_url]/admin/config/people/registration-restriction
