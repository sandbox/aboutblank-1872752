<?php
/**
 * @file
 * Specifies rules for restricting user registration based on email domains.
 */

/**
 * Implements hook_help().
 */
function registration_restriction_help($path, $arg) {
  switch ($path) {
    case 'admin/help#registration_restriction':
      $text = '<p>' . t('This module allows a site admin to restrict user registration based on whitelisting or blacklisting particular email domains.') . '</p>';
      $text .= '<p>' . t('An example use case is a school or organization that wants to restrict account creation to emails from their own domain.') . '</p>';

      $text .= '<p><strong>' . t('Warning:') . '</strong>' . ' ' . t('This module initializes with no valid emails. If you turn on this module and do not configure a list of valid email domains, no one will be able to register for your site!');

      return $text;
  }
}

/**
 * Builds administrative menu.
 * 
 * Used for configuring Registration Restriction module.
 */
function registration_restriction_admin() {
  $form = array();

  $form['registration_restriction_listed_domains'] = array(
    '#type' => 'textarea',
    '#title' => t('Email domains'),
    '#default_value' => variable_get('registration_restriction_listed_domains'),
    '#description' => t('Enter in email domains (the part after the "@" symbol). Seperate each with a comma, i.e. "drexel.edu, umass.edu."'),
    '#required' => TRUE,
  );

  $form['registration_restriction_type'] = array(
    '#type' => 'checkbox',
    '#title' => t('Blacklist listed domains'),
    '#description' => t('Check to blacklist these email domains (i.e. allow all domains EXCEPT ones listed here). The default is whitelist.'),
    '#default_value' => variable_get('registration_restriction_type', 1),
  );

  $form['registration_restriction_error_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Invalid email domain message'),
    '#default_value' => variable_get('registration_restriction_error_message', "That is not a valid email."),
    '#size' => 100,
    '#description' => t('This text is displayed when a user puts in an invalid email domain'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Implements hook_menu().
 */
function registration_restriction_menu() {
  $items = array();

  $items['admin/config/people/registration-restriction'] = array(
    'title' => 'Registration Restriction Settings',
    'description' => 'Settings for restricting user registration to specified email domains',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('registration_restriction_admin'),
    'access arguments' => array('administer users'),
  );

  return $items;
}

/**
 * Helper function to remove whitespace from a string.
 */
function registration_restriction_trim_value(&$value) {
  $value = trim($value);
}

/**
 * Helper function for checking if an email domain is valid. 
 * 
 * Checks based on the user configuration.
 */
function registration_restriction_is_restricted_domain($domain, $listed_domains) {
  switch (variable_get('registration_restriction_type')) {
    case (1):
      if (in_array($domain, $listed_domains)) {
        return TRUE;
      }
      break;

    default:
      if (!in_array($domain, $listed_domains)) {
        return TRUE;
      }
  };
}

/**
 * Implements hook_form_alter().
 *
 * Adds form validation to user registration form.
 */
function registration_restriction_form_alter(&$form, &$form_state, $form_id) {

  // Make sure this is the user registration form.
  if ($form_id == 'user_register_form') {
    // This adds your custom validation function to the form validation array.
    $form['#validate'][] = 'registration_restriction_user_register_validation';
  }
}

/**
 * Builds the form validation.
 * 
 * Checks the submitted email domain against this module's configuration.
 */
function registration_restriction_user_register_validation($form, &$form_state) {

  // Get valid domains from module configuration form.
  $listed_domains = explode(",", variable_get('registration_restriction_listed_domains'));
  array_walk($listed_domains, 'registration_restriction_trim_value');

  // Get domain from the user registration form.
  $email = $form_state['values']['mail'];
  $split_index = strpos($email, '@') + 1;
  $email_length = drupal_strlen($email);
  $domain = drupal_substr($email, $split_index, $email_length);

  // Display error if not valid domain.
  if (registration_restriction_is_restricted_domain($domain, $listed_domains)) {

    $message = t('@message', array('@message' => variable_get('registration_restriction_error_message')));

    form_set_error('mail', $message);
  }
}
